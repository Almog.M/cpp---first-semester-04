#include "FileStream.h"
#include <stdio.h>

FileStream::FileStream(FILE * stream)
{
	_stream = stream;
}

FileStream::~FileStream()
{
	//The destructor of the parent is calling here, so we do not need to remove the _stream obj here.
}
