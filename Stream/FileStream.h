#pragma once
#include <stdio.h>
#include "OutStream.h"

class FileStream : public OutStream
{
public:
	FileStream(FILE* stream); //Inits the stream of writing.
	~FileStream(); //Destruct the stream.

};