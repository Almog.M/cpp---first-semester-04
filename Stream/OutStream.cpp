#include "OutStream.h"
#include <stdio.h>

OutStream::OutStream()
{
	_stream = stdout;
}

OutStream::~OutStream()
{
	fclose(_stream);
}

OutStream& OutStream::operator<<(const char *str)
{
	fprintf(_stream, "%s", str);
	return *this;
}

OutStream& OutStream::operator<<(int num)
{
	fprintf(_stream, "%d", num);
	return *this;
}

OutStream& OutStream::operator<<(void(*pf)(FILE*))
{
	pf(_stream);
	return *this;
}


void endline(FILE* stream)
{
	fprintf(stream, "\n");
}
