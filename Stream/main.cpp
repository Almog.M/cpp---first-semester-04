#include "OutStream.h"
#include "FileStream.h"
#include <stdio.h>

int main(int argc, char **argv)
{
	/*OutStream os;
	os << "I am the Doctor and I'm " << 1500 << " years old" << endline;
	*/
	FILE* file = fopen("Paragraph B test.txt", "wb");
	FileStream fs(file);
	fs << "I am the Doctor and I'm " << 1500 << " years old" << endline;

	getchar();
	return 0;
}
